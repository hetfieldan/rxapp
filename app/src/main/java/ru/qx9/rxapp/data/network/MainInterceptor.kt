package ru.qx9.rxapp.data.network

import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager
import okhttp3.Interceptor
import okhttp3.Response
import ru.qx9.rxapp.addHeaders
import java.io.IOException
import javax.inject.Inject


class MainInterceptor constructor(private val mContext: Context) : Interceptor {
    
    private val isConnected: Boolean
        get() {
            val connectivityManager =
                mContext.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = connectivityManager.activeNetworkInfo
            return netInfo != null && netInfo.isConnected
        }

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        if (!isConnected) {
            throw NoConnectException()
        }

        val url = chain.request()
            .url
            .newBuilder()
            .build()

        val request = chain.request()
            .newBuilder()
            .url(url)
            .addHeaders(
                Pair("User-Agent", "RXApp/1.0 ( hetfieldan@yandex.ru )")
            )
            .build()

        return chain.proceed(request)
    }
}
