package ru.qx9.rxapp.dagger

import androidx.appcompat.app.AppCompatActivity
import dagger.BindsInstance
import dagger.Component
import ru.qx9.rxapp.BaseApplication
import ru.qx9.rxapp.dagger.modules.AppModule
import ru.qx9.rxapp.dagger.modules.NetworkModule
import ru.qx9.rxapp.dagger.modules.RoomModule
import ru.qx9.rxapp.dagger.modules.ViewModelModule
import ru.qx9.rxapp.ui.MainActivity
import javax.inject.Singleton


@Singleton
@Component(modules = [
    AppModule::class,
    ViewModelModule::class,
    NetworkModule::class,
    RoomModule::class
])
interface AppComponent {

    @Component.Builder
    interface Builder {
        fun build(): AppComponent

        @BindsInstance
        fun bindApplication(app: BaseApplication): Builder

        @BindsInstance
        fun bindActivity(activity: AppCompatActivity): Builder
    }

    fun injectMainActivity(activity: MainActivity): MainActivity
}
