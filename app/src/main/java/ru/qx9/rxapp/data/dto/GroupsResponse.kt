package ru.qx9.rxapp.data.dto

import com.google.gson.annotations.SerializedName
import ru.qx9.rxapp.data.room.GroupsTable

class GroupsResponse {

    @SerializedName("release-group-count")
    var releaseGroupCount: Int? = null

    @SerializedName("release-groups")
    var releaseGroups: List<GroupsTable>? = null
}
