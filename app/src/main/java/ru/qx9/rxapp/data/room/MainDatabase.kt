package ru.qx9.rxapp.data.room


import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(
    entities = [
        GroupsTable::class
    ],
    version = 30,
    exportSchema = false
)
@TypeConverters(MyTypeConverters::class)

abstract class MainDatabase: RoomDatabase() {
    abstract val groupsDao: GroupsDao
}
