package ru.qx9.rxapp.data.room


import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName


@Entity(tableName = "groups_table")
data class GroupsTable (
    var title: String? = null,

    @SerializedName("first-release-date")
    var firstReleaseDate: String? = null
) {
    @PrimaryKey(autoGenerate = true)
    var primaryKey: Int? = null
}
