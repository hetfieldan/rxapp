package ru.qx9.rxapp.dagger.modules

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import okhttp3.ConnectionPool
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.qx9.rxapp.BaseApplication
import ru.qx9.rxapp.data.network.GroupsAPI
import ru.qx9.rxapp.data.network.MainInterceptor
import ru.qx9.rxapp.data.repo.GroupsRepo
import ru.qx9.rxapp.data.room.GroupsDao
import ru.qx9.rxapp.data.room.MainDatabase
import ru.qx9.rxapp.ui.GroupsAdapter
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
class RoomModule {
    @Singleton
    @Provides
    fun provideDB(context: BaseApplication): MainDatabase =
        Room.databaseBuilder(
            context,
            MainDatabase::class.java,
            "main_database"
        )
        .fallbackToDestructiveMigration()
        .build()

    @Singleton
    @Provides
    fun provideCarsDAO(db: MainDatabase): GroupsDao = db.groupsDao
}
