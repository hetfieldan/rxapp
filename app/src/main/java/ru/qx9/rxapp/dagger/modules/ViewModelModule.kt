package ru.qx9.rxapp.dagger.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import ru.qx9.rxapp.viewmodels.ViewModelFactory
import ru.qx9.rxapp.dagger.keys.ViewModelKey
import ru.qx9.rxapp.viewmodels.GroupsVM


@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(GroupsVM::class)
    abstract fun bindCarsVM(viewModel: GroupsVM): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
