package ru.qx9.rxapp.data.network

import io.reactivex.Single
import retrofit2.http.*
import ru.qx9.rxapp.data.dto.GroupsResponse

interface GroupsAPI {
    @GET("release-group?artist=410c9baf-5469-44f6-9852-826524b80c61&type=album|ep&fmt=json")
    fun getGroupsResponse(@QueryMap queryMap: Map<String, String>): Single<GroupsResponse>
}
