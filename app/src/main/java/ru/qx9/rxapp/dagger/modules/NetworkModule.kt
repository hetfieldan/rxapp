package ru.qx9.rxapp.dagger.modules

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import okhttp3.ConnectionPool
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.qx9.rxapp.BaseApplication
import ru.qx9.rxapp.data.network.GroupsAPI
import ru.qx9.rxapp.data.network.MainInterceptor
import ru.qx9.rxapp.data.repo.GroupsRepo
import ru.qx9.rxapp.data.room.GroupsDao
import ru.qx9.rxapp.data.room.MainDatabase
import ru.qx9.rxapp.ui.GroupsAdapter
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
class NetworkModule {
    @Singleton
    @Provides
    fun provideSongsAPI(context: BaseApplication): GroupsAPI = Retrofit.Builder()
        .baseUrl("https://musicbrainz.org/ws/2/")
        .client(OkHttpClient.Builder()
            .connectTimeout(30000, TimeUnit.MILLISECONDS)
            .writeTimeout(30000, TimeUnit.MILLISECONDS)
            .readTimeout(30000, TimeUnit.MILLISECONDS)
            .connectionPool(ConnectionPool(5, 60, TimeUnit.SECONDS))
            .addInterceptor(MainInterceptor(context))
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
            .build())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(GroupsAPI::class.java)
}
