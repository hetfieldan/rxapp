package ru.qx9.rxapp.data.network

import java.io.IOException

class NoConnectException : IOException() {
    override val message: String?
        get() = "Нет соединения"
}
