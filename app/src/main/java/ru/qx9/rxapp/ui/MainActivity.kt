package ru.qx9.rxapp.ui

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import ru.qx9.rxapp.R
import ru.qx9.rxapp.data.room.GroupsTable
import ru.qx9.rxapp.getAppComponentBuilder
import ru.qx9.rxapp.viewmodels.GroupsVM
import javax.inject.Inject


class MainActivity : AppCompatActivity() {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: GroupsVM by viewModels { viewModelFactory }

    @Inject lateinit var adapter: GroupsAdapter
    @Inject lateinit var layoutManager: LinearLayoutManager
    @Inject lateinit var compositeDisposable: CompositeDisposable

    private lateinit var rvMovieList: RecyclerView
    private lateinit var footer: ProgressBar

    private var loading = false
    private var lastVisibleItem = 1
    private var totalItemCount = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViews()
        getAppComponentBuilder().bindActivity(this).build().injectMainActivity(this)

        rvMovieList.layoutManager = layoutManager
        rvMovieList.adapter = adapter

        subscribeOnContent()
        addNextPageListener()
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        super.onDestroy()
    }

    private fun initViews() {
        rvMovieList = findViewById(R.id.rv_movie_list)
        footer = findViewById(R.id.footer)
    }

    private fun addNextPageListener() {
        rvMovieList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                loadNextPageIfNecessary()
            }
        })
    }

    private fun loadNextPageIfNecessary() {
        totalItemCount = layoutManager.itemCount
        lastVisibleItem = layoutManager.findLastVisibleItemPosition()
        if (!loading && totalItemCount <= lastVisibleItem + VISIBLE_THRESHOLD) {
            viewModel.loadNextPage()
            loading = true
        }
    }

    private fun subscribeOnContent() {
        val disposable: Disposable = viewModel.getPageFlowable { showProgress() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { items -> refreshAdapterContent(items) }
        compositeDisposable.add(disposable)
        viewModel.loadPage()
    }

    private fun showProgress() {
        loading = true
        footer.visibility = View.VISIBLE
    }

    private fun refreshAdapterContent(items: List<GroupsTable>) {
        adapter.addItemsBatch(items)
        adapter.notifyDataSetChanged()
        loading = false
        footer.visibility = View.INVISIBLE
    }

    companion object {
        const val VISIBLE_THRESHOLD = 1
    }
}
