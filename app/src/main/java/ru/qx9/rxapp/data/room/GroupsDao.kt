package ru.qx9.rxapp.data.room

import androidx.lifecycle.LiveData
import androidx.room.*
import io.reactivex.Single


@Dao
interface GroupsDao {
    companion object {
        const val TABLE_NAME = "groups_table"
    }

    @Insert
    fun insertItems(items: List<GroupsTable>): List<Long>

    @Query("SELECT * FROM $TABLE_NAME")
    fun getItemsLive(): LiveData<List<GroupsTable>>

    @Query("SELECT * FROM $TABLE_NAME LIMIT 20 OFFSET (20 * (:page - 1))")
    fun getPage(page: Int): Single<List<GroupsTable>>

    @Query("DELETE FROM $TABLE_NAME")
    fun clear()

    @Transaction
    fun refreshItems(items: List<GroupsTable>): List<Long> {
        clear()
        return insertItems(items)
    }
}
