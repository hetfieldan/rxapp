package ru.qx9.rxapp

import android.content.Context
import android.util.Log
import io.reactivex.Observable
import okhttp3.Request

fun Context.getAppComponentBuilder() = (applicationContext as BaseApplication).getAppComponentBuilder()

fun <T> Observable<List<T>>.changeObserverIfEmpty(alterSource: Observable<List<T>>): Observable<List<T>> {
    return this.concatMap { if (it.isEmpty()) alterSource else Observable.fromArray(it) }
}

fun Any.mylog(message: Any?) {
    val className = this::class.java.simpleName
    Log.e("MYLOG $className", message?.toString() ?: "message is null")
}

fun Request.Builder.addHeaders(vararg pairs: Pair<String, String>): Request.Builder {
    for ((title, value) in pairs) {
        addHeader(title, value)
    }
    return this
}
