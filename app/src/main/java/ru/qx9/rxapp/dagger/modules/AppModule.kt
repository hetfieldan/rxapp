package ru.qx9.rxapp.dagger.modules

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import okhttp3.ConnectionPool
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.qx9.rxapp.BaseApplication
import ru.qx9.rxapp.data.network.GroupsAPI
import ru.qx9.rxapp.data.network.MainInterceptor
import ru.qx9.rxapp.data.repo.GroupsRepo
import ru.qx9.rxapp.data.room.GroupsDao
import ru.qx9.rxapp.data.room.MainDatabase
import ru.qx9.rxapp.ui.GroupsAdapter
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
class AppModule {
    @Singleton
    @Provides
    fun provideGroupsRepo(api: GroupsAPI, dao: GroupsDao): GroupsRepo = GroupsRepo(api, dao)

    @Provides
    fun provideCompositeDisposable() = CompositeDisposable()

    @Provides
    fun provideGroupsAdapter() = GroupsAdapter(mutableListOf())

    @Provides
    fun provideLayoutManager(activity: AppCompatActivity) = LinearLayoutManager(activity)
}
