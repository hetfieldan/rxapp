package ru.qx9.rxapp

import android.text.Html
import ru.qx9.rxapp.data.room.GroupsTable
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

fun formatGroupItemName(groupsTable: GroupsTable?): String {
    groupsTable ?: return ""
    return groupsTable.run {
        StringBuilder()
            .append(title ?: "")
            .append(if (firstReleaseDate != null) ", $firstReleaseDate" else "")
            .toString()
    }
}
