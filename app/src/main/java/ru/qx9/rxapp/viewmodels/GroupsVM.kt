package ru.qx9.rxapp.viewmodels

import androidx.lifecycle.ViewModel
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.processors.PublishProcessor
import io.reactivex.schedulers.Schedulers
import ru.qx9.rxapp.changeObserverIfEmpty
import ru.qx9.rxapp.data.room.GroupsTable
import ru.qx9.rxapp.data.repo.GroupsRepo
import ru.qx9.rxapp.mylog
import javax.inject.Inject

class GroupsVM @Inject constructor(
    private val repo: GroupsRepo,
    private val disposables: CompositeDisposable
): ViewModel() {

    private var pageNumber = 1
    private val paginator: PublishProcessor<Int> = PublishProcessor.create()

    fun loadPage() = paginator.onNext(pageNumber)
    fun loadNextPage() = paginator.onNext(++pageNumber)

    fun getPageFlowable(showProgress: () -> Unit): Flowable<List<GroupsTable>> {
        return paginator
            .doOnNext { showProgress() }
            .concatMapSingle { page -> getPageSingle(page) }
    }

    private fun getPageSingle(page: Int): Single<List<GroupsTable>> {
        return Single.fromObservable(repo.getItemsFromDBSingle(page)
            .subscribeOn(Schedulers.io())
            .toObservable()
            .changeObserverIfEmpty(
                getPageFromNetworkSingle(page).toObservable()
            )
        )
    }

    private fun getPageFromNetworkSingle(page: Int): Single<List<GroupsTable>> {
        return repo.getSongsFromNetworkSingle(page)
            .subscribeOn(Schedulers.io())
            .map {
                mylog("getPageFromNetworkSingle: $page, ${it.size}")
                startCaching(it)
                it
            }
    }

    private fun startCaching(items: List<GroupsTable>) {
        mylog("startCaching: ${items.size}")
        disposables.add(
            Observable.fromCallable { repo.cacheGroups(items) }
                .subscribeOn(Schedulers.io())
                .doOnError { throwable -> mylog("блять fromCallable " + throwable.localizedMessage) }
                .subscribe()
        )
    }
}
