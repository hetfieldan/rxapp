package ru.qx9.rxapp.data.repo

import io.reactivex.Single
import ru.qx9.rxapp.data.network.GroupsAPI
import ru.qx9.rxapp.data.room.GroupsDao
import ru.qx9.rxapp.data.room.GroupsTable
import ru.qx9.rxapp.mylog
import javax.inject.Inject


class GroupsRepo @Inject constructor(private val api: GroupsAPI, private val dao: GroupsDao) {

    fun getSongsFromNetworkSingle(currentPage: Int): Single<List<GroupsTable>> {
        val limit = 10
        val offset = limit * (currentPage - 1)
        val queryMap = hashMapOf(
            "limit" to "$limit",
            "offset" to "$offset"
        )
        return api.getGroupsResponse(queryMap)
            .map { resp ->
                mylog("блять resp: ${resp.releaseGroups?.size}")
                resp.releaseGroups
            }
    }

    fun getItemsFromDBSingle(page: Int): Single<List<GroupsTable>> {
        return dao.getPage(page)
    }

    fun cacheGroups(items: List<GroupsTable>) {
        dao.insertItems(items)
    }

//    fun clearItems() {
//        Observable.fromCallable { dao.clear() }
//            .subscribeOn(Schedulers.io())
//            .subscribe()
//    }

    fun refreshItems(items: List<GroupsTable>) {
        dao.refreshItems(items)
    }
}
