package ru.qx9.rxapp

import android.app.Application
import ru.qx9.rxapp.dagger.AppComponent
import ru.qx9.rxapp.dagger.DaggerAppComponent

class BaseApplication : Application() {

    private lateinit var appComponentBuilder: AppComponent.Builder

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this

        appComponentBuilder = DaggerAppComponent.builder().bindApplication(this)
    }

    fun getAppComponentBuilder() = appComponentBuilder

    companion object {
        lateinit var INSTANCE: BaseApplication
    }
}
