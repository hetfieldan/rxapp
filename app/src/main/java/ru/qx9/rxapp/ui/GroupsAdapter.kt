package ru.qx9.rxapp.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.BitmapImageViewTarget
import ru.qx9.rxapp.*
import ru.qx9.rxapp.data.room.GroupsTable
import kotlin.random.Random


class GroupsAdapter(private val items: MutableList<GroupsTable>):
    RecyclerView.Adapter<GroupsAdapter.CarsItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarsItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.item_groups, parent, false)
        return CarsItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: CarsItemViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size

    fun addItemsBatch(itemsBatch: List<GroupsTable>) {
        items.addAll(itemsBatch)
    }

    class CarsItemViewHolder(view: View): RecyclerView.ViewHolder(view) {

        private var itemNameTextView: TextView = view.findViewById(R.id.itemNameTextView)
        private var itemImageView: ImageView = view.findViewById(R.id.itemImageView)

        fun bind(item: GroupsTable?) {
            itemNameTextView.text = formatGroupItemName(item)

            try {
                GlideApp.with(itemView.context.applicationContext)
                    .asBitmap()
                    .load("https://picsum.photos/96?fakeParam=${Random.nextInt(1000000)}")
                    .apply(RequestOptions().apply {
                        placeholder(R.color.imageBackground)
                        centerInside()
                        timeout(30000)
                    })
                    .into(BitmapImageViewTarget(itemImageView))
            } catch (e: Exception) {
                mylog("glide не смог загрузить фотку")
            }
        }
    }
}
